#!/usr/bin/env python
"""Setup for gl-snippet."""

import setuptools
from gl_snippet import version  # noqa

# The setup.cfg contains all common option values
setuptools.setup(
    version=version.__version__,
    install_requires=[
        'python-gitlab',
        'click'
    ],

    entry_points={
        'console_scripts': [
            "gl-snippet=gl_snippet:cli",
        ],
    },
)
